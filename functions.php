<?php

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {

	$nectar_theme_version = nectar_get_theme_version();

	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'), $nectar_theme_version);

	if ( is_rtl() )
	wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );

	if (is_front_page()) {
		wp_enqueue_script( 'salient-child-home', get_stylesheet_directory_uri() . '/js/home.js', [ 'jquery' ] );
	}
}

/* Overrides
-----------------------------------------------------------------------*/

//CUSTOM FONTS
function salient_redux_custom_fonts( $custom_fonts ) {
	return array(
		'Custom Fonts' => array(
			'Proxima Nova' => "proxima-nova, sans-serif",
			'Palatino' => "Palatino, Georgia, serif"
		)
	);
}
add_filter( "redux/salient_redux/field/typography/custom_fonts", "salient_redux_custom_fonts" );

//UPLOAD SVG SUPPORT
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//ADMIN THEME

	function custom_login_css() {
		echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/login.css" />';
		}
	add_action('login_head', 'custom_login_css');

	/* Remove Forgot Password */

	function remove_lostpassword_text ( $text ) {
		if ($text == 'Lost your password?'){$text = '';}
		return $text;
		}
	add_filter( 'gettext', 'remove_lostpassword_text' );

	/* No Shake */

	function my_login_head() {
		remove_action('login_head', 'wp_shake_js', 12);
		}
	add_action('login_head', 'my_login_head');

	/* Logo Url */

	function my_login_logo_url() {
		return get_bloginfo( 'url' );
		}
	add_filter( 'login_headerurl', 'my_login_logo_url' );

	function my_login_logo_url_title() {
		return 'Nurturing Nutrition';
		}
	add_filter( 'login_headertitle', 'my_login_logo_url_title' );
