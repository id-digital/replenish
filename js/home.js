jQuery(document).ready(function($) {
    let tabs = $('.homepage-books .wpb_tabs_nav li');
    console.log(tabs);
    let key = 0;
    let interval = setInterval(() => {
        $(tabs[key]).children('a').trigger('click');
        key += 1;
        if (key >= tabs.length)
            key = 0;
    }, 6000);
    tabs.children('a').on('click', (e) => {
        if (interval && e.originalEvent !== undefined) {
            clearInterval(interval);
            interval = null;
        }
    });
});